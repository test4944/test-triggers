# Maven web application project

### Clean fresh application code is available in clean branch and clean tag
To clone the clean branch
```
git clone -b clean https://github.com/vigneshsweekaran/hello-world.git
```
### To generate the package
```
mvn clean package
```
### War file is generated in target/hello-world.war

### Terraform Commands
#### Terraform Init
```
terraform init \
    -backend-config="address=https://gitlab.com/api/v4/projects/30252780/terraform/state/hello-world" \
    -backend-config="lock_address=https://gitlab.com/api/v4/projects/30252780/terraform/state/hello-world/lock" \
    -backend-config="unlock_address=https://gitlab.com/api/v4/projects/30252780/terraform/state/hello-world/lock" \
    -backend-config="username=vigneshsweekaran" \
    -backend-config="password=<YOUR-ACCESS-TOKEN>" \
    -backend-config="lock_method=POST" \
    -backend-config="unlock_method=DELETE" \
    -backend-config="retry_wait_min=5"
```

##### Terraform plan
```
terraform plan
```

##### Terraform apply
```
terraform apply -auto-approve
```
